<?php

use MediaWiki\MediaWikiServices;

class MobileDetectNonsa {

	/**
	 * @param Parser $parser
	 *
	 * @throws MWException
	 */
	public static function onParserFirstCallInit( Parser &$parser ) {
		$parser->setHook( 'mobileonly', [ self::class, 'mobileonly' ] );
		$parser->setHook( 'nomobile', [ self::class, 'nomobile' ] );
	}

	public static function nomobile( $input, array $args, Parser $parser, PPFrame $frame ) {
		$t = MobileDetectParser::tags( 'nomobile' );
		return $t[0] . $parser->recursiveTagParse( $input, $frame ) . $t[1];
	}

	public static function mobileonly( $input, array $args, Parser $parser, PPFrame $frame ) {
		$t = MobileDetectParser::tags( 'mobileonly' );
		return $t[0] . $parser->recursiveTagParse( $input, $frame ) . $t[1];
	}

	public static function onParserOutputPostCacheTransform( ParserOutput $parserOutput, string &$text,
		array &$options ) {
		/** @var MobileContext $mobileContext */
		$mobileContext = MediaWikiServices::getInstance()->getService( 'MobileFrontend.Context' );
		$isMobile = $mobileContext->shouldDisplayMobileView();

		$mdParser = new MobileDetectParser( $text );
		$text = $mdParser->getText( $isMobile );
	}

}