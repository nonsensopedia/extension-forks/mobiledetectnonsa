<?php

class MobileDetectParser {
	public const MARKER_PREFIX = '<!--```UNIQ-';
	public const MARKER_SUFFIX = '-QINU```-->';

	/**
	 * @var MdTagNode
	 */
	private $tree;

	/**
	 * MobileDetectParser constructor.
	 *
	 * @param string $text
	 */
	public function __construct( string $text ) {
		$tags = array_merge( self::tags( 'mobileonly' ), self::tags( 'nomobile' ) );
		$tags = array_map( function ( $x ) { return preg_quote( $x ); }, $tags );
		$rx = '/(' . implode( ')|(', $tags ) . ')/';
		$this->tree = new MdTagNode( null, 'root' );

		if ( preg_match_all( $rx, $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE ) ) {
			$ix = 0;
			$currentNode = $this->tree;

			foreach ( $matches as $match ) {
				$matchOffset = $match[0][1];
				$matchLen = strlen( $match[0][0] );
				$prevText = substr( $text, $ix, $matchOffset - $ix );

				if ( $prevText ) {
					$currentNode->children[] = new MdTextNode( $currentNode, $prevText );
				}

				if ( !empty( $match[1][0] ) ) {
					// mobileonly start
					$newNode = new MdTagNode( $currentNode, 'mobileonly' );
					$currentNode->children[] = $newNode;
					$currentNode = $newNode;
 				} else if ( !empty( $match[3][0] ) ) {
					// nomobile start
					$newNode = new MdTagNode( $currentNode, 'nomobile' );
					$currentNode->children[] = $newNode;
					$currentNode = $newNode;
				} else {
					// mobileonly / nomobile end
					if ( $currentNode->parent === null ) {
						wfDebugLog( 'MobileDetect', 'Invalid MD markup.' );
					} else {
						$currentNode = $currentNode->parent;
					}
				}

				$ix = $matchOffset + $matchLen;
			}

			$remainingText = substr( $text, $ix );
			$this->tree->children[] = new MdTextNode( $this->tree, $remainingText );
		} else {
			$this->tree->children[] = new MdTextNode( $this->tree, $text );
		}
	}

	public function getText( string $mode ) {
		return $this->tree->getText( $mode );
	}

	public static function makeRegex( string $name ) : string {
		$t = self::tags( $name );

		return '/' . preg_quote( $t[0],
				'/' ) . '(.*?)' . preg_quote( $t[1],
				'/' ) . '/s';
	}

	public static function tags( string $name ) : array {
		return [ self::MARKER_PREFIX . $name . '-start' . self::MARKER_SUFFIX,
			self::MARKER_PREFIX . $name . '-end' . self::MARKER_SUFFIX ];
	}
}