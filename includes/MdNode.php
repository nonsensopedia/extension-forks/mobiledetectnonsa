<?php

abstract class MdNode implements JsonSerializable {
	/**
	 * @var MdNode|null
	 */
	public $parent;

	public abstract function getText( bool $isMobile ) : string;
}

class MdTagNode extends MdNode {
	/**
	 * @var MdNode[]
	 */
	public $children = [];

	/**
	 * @var string
	 */
	public $type;

	public function __construct( ?MdNode $parent, string $type ) {
		$this->parent = $parent;
		$this->type = $type;
	}

	public function jsonSerialize() {
		return [
			'type' => $this->type,
			'children' => $this->children
		];
	}

	public function getText( bool $isMobile ) : string {
		if ( ( $isMobile && $this->type === 'nomobile' ) ||
			( !$isMobile && $this->type === 'mobileonly' ) ) {
			return '';
		}

		$text = '';
		foreach ( $this->children as $child ) {
			$text .= $child->getText( $isMobile );
		}
		return $text;
	}
}

class MdTextNode extends MdNode {
	/**
	 * @var string
	 */
	public $text;

	public function __construct( MdNode $parent, string $text ) {
		$this->parent = $parent;
		$this->text = $text;
	}

	public function jsonSerialize() {
		return [ 'text' => str_replace( "\n", ' ', $this->text ) ];
	}

	public function getText( bool $isMobile ) : string {
		return $this->text;
	}
}